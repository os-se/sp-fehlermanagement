package de.hfu;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Beispiele für Unit-Tests
 */
@DisplayName("Test der Klasse App")
public class AppTest 
{

    /**
     * Wird einmal vor allen Tests dieser Klasse aufgerufen
     */
    @BeforeAll
    static void initAll() {
    }

    /**
     * Wird jeweils vor den Tests dieser Klasse aufgerufen
     */
    @BeforeEach
    void init() {
    }

    /**
     * Wird immer erfolgreich sein
     */
    @Test
    void succeedingTest() {
        assertEquals(1, 1, "ist immer gleich");
    }

    /**
     * Wird niemals erfolgreich sein
     * Deshalb besser deaktiviert
     */
    @Test
    @Disabled("kein sinnvoller Test, deshalb deaktiviert")
    void failingTest() {
        fail("dieser Test schlägt fehl");
    }

    /**
     * Wird jeweils nach den Tests dieser Klasse aufgerufen
     */
    @AfterEach
    void tearDown() {
    }

    /**
     * Wird einmal nach allen Tests dieser Klasse aufgerufen
     */
    @AfterAll
    static void tearDownAll() {
    }

}