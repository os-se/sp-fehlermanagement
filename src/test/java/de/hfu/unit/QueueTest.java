package de.hfu.unit;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class QueueTest {
    @Test
    public void testQueue() {
        Queue queue1 = new Queue(1);
        Queue queue3 = new Queue(3);

        try {
            queue1.dequeue();
            fail("Dequeue worked on an empty queue");
        } catch (IllegalStateException e) {
            assertTrue(true);
        } catch (Exception e) {
            fail("Wrong exception thrown");
        }

        queue1.enqueue(1);
        int got = queue1.dequeue();
        int expected = 1;
        assertEquals(expected, got, "Expected: " + expected + ", got: " + got);

        queue1.enqueue(1);
        queue1.enqueue(2);
        got = queue1.dequeue();
        expected = 2;
        assertEquals(expected, got, "Expected: " + expected + ", got: " + got);

        for (int i = 0; i < 5; i++) {
            queue3.enqueue(i);
        }

        got = queue3.dequeue();
        expected = 2;
        assertEquals(expected, got, "Expected: " + expected + ", got: " + got);
    }
}
