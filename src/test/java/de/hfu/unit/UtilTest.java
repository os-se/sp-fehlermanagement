package de.hfu.unit;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class UtilTest {
    @Test
    public void testIstErstesHalbJahr() {
        for (int i = 1; i <= 12; i++) {
            if (i <= 6) {
                assertTrue(Util.istErstesHalbjahr(i), "Asser true failed for i == " + i);
            }else {
                assertFalse(Util.istErstesHalbjahr(i), "Assert false failed for i == " + i);
            }
        }

        try {
            Util.istErstesHalbjahr(0);
            fail("Failed for 0");
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch(Exception e) {
            fail("Failed for 0 with a wrong exception");
        }

        try {
            Util.istErstesHalbjahr(13);
            fail("Failed for >12 ith a wrong exception");
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch(Exception e) {
            fail("Failed for >12 with a wrong exception");
        }
    }
}
