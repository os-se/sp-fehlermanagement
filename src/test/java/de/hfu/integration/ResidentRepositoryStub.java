package de.hfu.integration;
import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import java.util.*;

public class ResidentRepositoryStub implements ResidentRepository {

    public List<Resident> getResidents() {
    Resident tom = new Resident("Tom", "Diego", "Examplestreet", "Examplingen", new Date(2000, 2, 20));
    Resident jenny = new Resident("Jenny", "Hexenschuss", "Hauptstraße", "Berliningen", new Date(2005, 5, 5));
    Resident gertrud = new Resident("Gertrud", "Schmitt", "Straße", "Stadt", new Date(1960, 6, 6));
    Resident gerhard = new Resident("Gerhard", "Schmitt", "Straße", "Stadt", new Date(1957, 3, 8));

    List<Resident> residents = Arrays.asList(tom, jenny, gertrud, gerhard);

    return residents;
    }
}
