package de.hfu.integration;

import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;
import de.hfu.integration.domain.Resident;
import java.util.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.easymock.EasyMock.*;

public class ResidentRepositoryTest {
    ResidentRepository stub = new ResidentRepositoryStub();
    
    @Test
    public void getFilteredResidentsList() {
        BaseResidentService bRS = new BaseResidentService();
        bRS.setResidentRepository(stub);

        List<Resident> filtered = bRS.getFilteredResidentsList(new Resident("", "", "", "", new Date(2000, 2, 20)));
        assertEquals(1, filtered.size(), "Date only - Size - Tom");
        assertEquals("Tom", filtered.get(0).getGivenName(), "Date only - Given Name - Tom");

        filtered = bRS.getFilteredResidentsList(new Resident("T*", "", "", "", new Date(2000, 2, 20)));
        assertEquals(1, filtered.size(), "Upper case - Size - Tom");
        assertEquals("Diego", filtered.get(0).getFamilyName(), "Upper case - Family Name - Tom");

        filtered = bRS.getFilteredResidentsList(new Resident("t*", "", "", "", new Date(2000, 2, 20)));
        assertEquals(1, filtered.size(), "Lower case - Size - Tom");
        assertEquals("Tom", filtered.get(0).getGivenName(), "Lower case - Given Name - Tom");

        filtered = bRS.getFilteredResidentsList(new Resident("", "", "", "", new Date(2005, 5, 5)));
        assertEquals(1, filtered.size(), "Date only - Size - Jenny");
        assertEquals("Jenny", filtered.get(0).getGivenName(), "Date only - Given Name - Jenny");

        filtered = bRS.getFilteredResidentsList(new Resident("Ger*", "", "", "", new Date(1960, 6, 6)));
        assertEquals(1, filtered.size(), "Upper case - Size - Gertrud");
        assertEquals("Schmitt", filtered.get(0).getFamilyName(), "Upper case - Family Name - Gertrud");

        filtered = bRS.getFilteredResidentsList(new Resident("ger*", "", "", "", null));
        assertEquals(2, filtered.size(), "Lower case - Size - Gertrud & Gerhard");
        assertEquals("Gertrud", filtered.get(0).getGivenName(), "Lower case - Given Name - Gertrud ");
        assertEquals("Gerhard", filtered.get(1).getGivenName(), "Lower case - Given Name - Gertrud ");
    }

    @Test
    public void getUniqueResidentTest() {
        BaseResidentService bRS = new BaseResidentService();
        bRS.setResidentRepository(stub);

        try {
            Resident unique = bRS.getUniqueResident(new Resident("", "", "", "", new Date(2000, 2, 20)));
            assertEquals("Tom", unique.getGivenName(), "Date only - Given Name - Tom");
        } catch(ResidentServiceException e) {
            fail("Exception thrown for Date only");
        } catch(Exception e) {
            fail("Wrongfully thrown Exception for Date only");
        }

        try {
            Resident unique = bRS.getUniqueResident(new Resident("Tom", "", "", "", new Date(2000, 2, 20)));
            assertEquals("Tom", unique.getGivenName(), "Date and Name - Given Name - Tom");
            assertEquals("Diego", unique.getFamilyName(), "Date and Name - Family Name - Tom");
        } catch(ResidentServiceException e) {
            fail("Exception thrown for Date and Name");
        } catch(Exception e) {
            fail("Wrongfully thrown Exception for Date and Name");
        }

        try {
            Resident unique = bRS.getUniqueResident(new Resident("Tom", "Diego", "Examplingen", "Hauptstraße", new Date(2000, 2, 20)));
            fail("Found Unique Resident, that doesn't exist");
        } catch(ResidentServiceException e) {
            assertEquals(e.getMessage(), "Suchanfrage lieferte kein eindeutiges Ergebnis!", "All - Exception");
        } catch (Exception e) {
            fail("Wrongfully thrown Exception for All - Exception");
        }

        try {
            Resident unique = bRS.getUniqueResident(new Resident("Tom", "Diego", "Examplestreet", "Examplingen", new Date(2000, 2, 20)));
            assertEquals("Diego", unique.getFamilyName(), "Date and Name - Family Name - Tom");
        } catch(ResidentServiceException e) {
            fail("Exception thrown for All");
        } catch (Exception e) {
            fail("Wrongfully thrown Exception for All");
        }

        try {
            Resident unique = bRS.getUniqueResident(new Resident("Ger*", "", "", "", null));
            fail("Found Unique Resident, that doesn't exist");
        } catch(ResidentServiceException e) {
            assertEquals(e.getMessage(), "Wildcards (*) sind nicht erlaubt!", "* - Exception");
        } catch (Exception e) {
            fail("Wrongfully thrown Exception for All - Exception");
        }
    }

    @Test
    public void mockTest() {
        Resident res1 = new Resident("Anna", "Lena", "Beistreet", "Beilen", new Date(2020, 2, 20));
        Resident res2 = new Resident("Jeremy", "Stein", "Beistreet", "Beilen", new Date(2000, 10, 2));
        Resident res3 = new Resident("Justin", "Mann", "Steet", "Brumm", new Date(2020, 2, 20));

        ResidentRepository mock = createMock(ResidentRepository.class);

        expect(mock.getResidents()).andReturn(Arrays.asList(res1, res2, res3));

        replay(mock);

        BaseResidentService bRS = new BaseResidentService();
        bRS.setResidentRepository(mock);

        List<Resident> filtered = bRS.getFilteredResidentsList(new Resident("J*", "", "", "", null));

        verify(mock);

        assertThat(filtered.size(), equalTo(2));
        // assertThat(filtered, contains(res2));
        // assertThat(filtered, contains(res3));
        assertTrue(filtered.contains(res2));
        assertTrue(filtered.contains(res3));
    }
}
