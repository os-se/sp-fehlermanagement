package de.hfu.integration.service;

import java.util.List;

import de.hfu.integration.domain.Resident;

/**
 * @author Stefan Betermieux
 */
// Used to get one or multiple residents from the database.
public interface ResidentService {

  Resident getUniqueResident(Resident filterResident) throws ResidentServiceException;

  List<Resident> getFilteredResidentsList(Resident filterResident);

}